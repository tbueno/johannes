require 'spec_helper'

describe Johannes::Generator do

  describe "#generate_chapter" do
    before do
      @outputFile = mock
      @outputFile.stub(:write)
      @outputFile.stub(:close)
    end

    let(:template) { double("template", :render => nil) }

    describe "generate_chapter"

      it "reads template file from template folders" do
        name = "file_name"
        Tilt.stub(:new => template)
        Johannes::Generator.stub(:save)
        File.should_receive(:join).with(Johannes::Configuration.templates_dir , "#{name}.html.erb").and_return(@outputFile)
        Johannes::Generator.generate_chapter(name)
      end
    end

    describe "generate_book" do
      it "throws exception when kindlegen is no defined in the path" do
        ENV['KINDLEGEN_HOME'] = nil
        Johannes::Generator.stub(:generate_toc)
        Johannes::Generator.stub(:generate_opf)
        expect {Johannes::Generator.generate_book }.to raise_error(RuntimeError, /Kindlegen/)
    end

  end
end
