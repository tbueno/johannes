require 'spec_helper'

describe Johannes::Element::Book do
 
  describe "initialization" do
  
    subject { Johannes::Element::Book.new }
    its(:table_of_contents){should_not be_nil} 
    its(:chapters){should == []}
  end
end


