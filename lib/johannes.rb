require 'rubygems'
require 'tilt'

module Johannes
  ROOT_PATH = File.join(File.dirname(__FILE__), '..')
  require File.dirname(__FILE__)+'/johannes/generator'
  require File.dirname(__FILE__)+'/johannes/configuration'
  require File.dirname(__FILE__)+'/johannes/element'
end

