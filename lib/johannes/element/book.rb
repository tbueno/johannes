module Johannes
  module Element
    class Book

      attr_accessor :chapters

      def initialize(config= Johannes::Configuration.book)
        @config = config
        @chapters = []
      end

      def table_of_contents
        "not nil"
      end

    end 
  end
end

