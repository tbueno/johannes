require 'yaml'
require 'singleton'

module Johannes
  class Configuration

    include Singleton

    CONFIG_FILE = File.join(ROOT_PATH, 'config', 'config.yml')

    attr_reader :conf

    def initialize
      @conf = YAML::load(File.open(CONFIG_FILE))
    end

    def self.output_dir
      configuration['output_folder']
    end

    def self.templates_dir
      configuration['templates_folder']
    end

    def self.book_title
      book['title']
    end

    def self.book
      configuration['book']
    end


    private
    def self.configuration
      Configuration.instance.conf
    end

  end
end
