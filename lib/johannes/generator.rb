module Johannes
  class Generator
    def self.generate_chapter(name = "chapter")
      output = render(:template_name => "#{name}.html.erb", :chapter_name => name)
      save(output, "#{name}.html")
    end

    def self.generate_book
      raise "Kindlegen home should be set in the PATH" unless ENV['KINDLEGEN_HOME']
      output = render(:template_name => 'book.html.erb' , :book_title => Configuration.book_title)
      save(output, 'book.html')
      generate_toc
      generate_opf
    end

    def self.build
      puts `kindlegen #{Configuration.output_dir}/book.html`
    end

    private

    def self.generate_opf
      output = render(Configuration.book.merge(:template_name => 'book.opf.erb'))
      save(output, "book.opf")
    end

    def self.generate_toc
      output = render(Configuration.book.merge(:template_name => 'toc.ncx.erb'))
      save(output, "toc.ncx")
    end

    def self.render(args = {})
      template = Tilt.new(File.join(Configuration.templates_dir, args[:template_name]))
      template.render("scope", args)
    end

    def self.save(content, name)
      outputFile = File.new(File.join( Configuration.output_dir, name), "w")
      outputFile.write(content)
      outputFile.close
    end
  end
end
