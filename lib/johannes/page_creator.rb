module Johannes
  class PageCreator
    def self.generate_book
      template = Tilt.new(File.join(TEMPLATE_FOLDER, 'book.erb'))
      s = "fake scope"
      output = template.render(s, :title => "My book ")

      outputFile = File.new(File.join(OUTPUT_FOLDER, 'book.html'), "w")
      outputFile.write(output)
      outputFile.close
    end
  end
end
